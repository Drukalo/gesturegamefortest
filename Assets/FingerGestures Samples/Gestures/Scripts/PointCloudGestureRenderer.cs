using UnityEngine;
using System.Collections;
using System.Threading;

[RequireComponent( typeof( LineRenderer ) )]
public class PointCloudGestureRenderer : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public PointCloudGestureTemplate GestureTemplate;

    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.useWorldSpace = false;
    }

    void Start()
    {
        if( GestureTemplate )
            Render( GestureTemplate );
		//Thread.Sleep(3000);
		//lineRenderer.SetVertexCount(0);

    }
	//PointCloudGestureTemplate temp;
    public void Blink()
    {
        animation.Stop();
        animation.Play();
		//Thread.Sleep(3000);
	//	int index = 1;
		//lineRenderer.SetPosition (index, Vector3(0, 1, 0));
		//lineRenderer.SetVertexCount(7);

			lineRenderer.SetVertexCount(0);
		//showGesture = false;
    }

	public void Show()
	{
		lineRenderer.SetVertexCount(1);
	}

	public void Dissapear()
	{
		//Thread.Sleep(3000);

	}
    public bool Render( PointCloudGestureTemplate template )
    {
		if (template.PointCount < 2)
			return false;

		lineRenderer.SetVertexCount (template.PointCount);

		for (int i = 0; i < template.PointCount; ++i)
			lineRenderer.SetPosition (i, template.GetPosition (i));


        return true;
    }
}
