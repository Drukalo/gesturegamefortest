﻿using UnityEngine;
using System.Collections;

public class ParticleScript : MonoBehaviour {
	
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			this.particleSystem.Play(false);

		}
		if (Input.GetMouseButton(0)) {
			Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
			this.particleEmitter.transform.position = mousePosition;
		}
		if (Input.GetMouseButtonUp(0)) {
			this.particleSystem.Stop ();
			}
	}
}
