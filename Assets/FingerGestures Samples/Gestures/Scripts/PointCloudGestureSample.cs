using UnityEngine;
using System.Collections.Generic;
using System.Threading;
using System.Timers;

[RequireComponent(typeof(PointCloudRegognizer))]
public class PointCloudGestureSample : SampleBase
{
    public PointCloudGestureRenderer GestureRendererPrefab;
    public float GestureScale = 8.0f;
    public Vector2 GestureSpacing = new Vector2( 1.25f, 1.0f );
    public int MaxGesturesPerRaw = 1;
	float levelTimeDifficult = 0f;
	float hintTime = 1f;
	float levelTime = 10f;
	bool gestureWasEntered = false;
	int lastIndex = -1;
	int pointCount =0;
	bool startFlag = false;
	ParticleSystem particle;

    List<PointCloudGestureRenderer> gestureRenderers;
    
    protected override void Start()
    {
		base.Start ();
		RenderGestureTemplates ();
	}
	void Update () {
		if (UI.StartGame != true) {
			if(gestureRenderers!=null){
				foreach(PointCloudGestureRenderer gr in gestureRenderers) 
				{
					gr.Blink();
				}
			}
			return;
		} else if (!startFlag) {
			RenderGestureTemplates ();
			if(gestureRenderers!=null){
			
			foreach(PointCloudGestureRenderer gr2 in gestureRenderers) 
			{
				gr2.Show();
			}
			startFlag = true;
			}
			return;
		}

		hintTime -= Time.deltaTime;
		levelTime -= Time.deltaTime;
		if (hintTime < 0) 
		{
			foreach(PointCloudGestureRenderer gr in gestureRenderers) 
			{
				gr.Blink();
			}
		} 
		if (levelTime < 0 && !gestureWasEntered) 
		{
			UI.StatusText = "Время вышло!";

		}
	}    
	
    void OnCustomGesture( PointCloudGesture gesture )
    {
		if (UI.StatusText == "Время вышло!"||hintTime+0.5f>0) {
			return;
		}
        PointCloudGestureRenderer gr = FindGestureRenderer( gesture.RecognizedTemplate );
        if (gr) {
			levelTimeDifficult += 0.5f;
			levelTime = 10f - levelTimeDifficult;
			if(levelTime <= 0.5f) {
				levelTime = 0.5f;
			}
			hintTime = 1f;
			RenderGestureTemplates();
			foreach(PointCloudGestureRenderer gr2 in gestureRenderers) 
			{
				gr2.Show();
			}
			pointCount+=1;
			UI.Title = "Колличество очков:"+ pointCount;
		}
    }

	void OnFingerDown( FingerDownEvent e ) 
	{
		UI.StatusText = string.Empty;
    }
   

    void RenderGestureTemplates()
    {
        gestureRenderers = new List<PointCloudGestureRenderer>();

        Transform gestureRoot = new GameObject( "Gesture Templates" ).transform;
        gestureRoot.parent = this.transform;
        gestureRoot.localScale = GestureScale * Vector3.one;

        PointCloudRegognizer recognizer = GetComponent<PointCloudRegognizer>();
        Vector3 pos = Vector3.zero;
        int gesturesOnRow = 0;
        int rows = 0;
        float rowWidth = 0;

		int index = 0;
		for(;;)
		{
			index = UnityEngine.Random.Range(0, recognizer.Templates.Count);
			if(index != lastIndex)
			{
				lastIndex = index;
				break;
			}
		}

		PointCloudGestureTemplate template = recognizer.Templates [index];

        PointCloudGestureRenderer gestureRenderer = Instantiate( GestureRendererPrefab, gestureRoot.position, gestureRoot.rotation ) as PointCloudGestureRenderer;
        gestureRenderer.GestureTemplate = template;
        gestureRenderer.name = template.name;
        gestureRenderer.transform.parent = gestureRoot;
        gestureRenderer.transform.localPosition = pos;
        gestureRenderer.transform.localScale = Vector3.one;
        
        pos.x += GestureSpacing.x;

        rowWidth = Mathf.Max( rowWidth, pos.x );

        if( ++gesturesOnRow >= 1 )
        {
            pos.y += GestureSpacing.y;
            pos.x = 0;
            gesturesOnRow = 0;
        }

        gestureRenderers.Add( gestureRenderer );

        Vector3 rootPos = Vector3.zero;
        rootPos.x -= GestureScale * 0.5f * ( rowWidth - GestureSpacing.x );

        if( rows > 0 )
            rootPos.y -= GestureScale * 0.5f * ( pos.y - GestureSpacing.y );

        gestureRoot.localPosition = rootPos;

    }

    PointCloudGestureRenderer FindGestureRenderer( PointCloudGestureTemplate template )
    {
        return gestureRenderers.Find( gr => gr.GestureTemplate == template );
    }

}
