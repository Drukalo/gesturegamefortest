using UnityEngine;
using System.Collections;

// iphone3 480 x 320
// iphone4 960 x 640
public class SampleUI : MonoBehaviour
{
    public GUISkin skin;
    
    public Color titleColor = Color.white;
    GUIStyle titleStyle;
    GUIStyle statusStyle;
    GUIStyle helpStyle;
	bool start =false;

	Rect topBarRect = new Rect( 0, -4, 600, 36 );
	Rect backButtonRect = new Rect( 240, 200, 150, 46 );
	Rect titleRect = new Rect( 250, 2, 400, 46 );
	//Rect helpButtonRect = new Rect( 515, 2, 80, 46 );
	//Rect statusTextRect = new Rect( 35, 336, 540, 60 );
	//Rect helpRect = new Rect( 50, 60, 500, 300 );

    string statusText = "";//"status text goes here";
    public string StatusText
    {
        get { return statusText; }
        set { statusText = value; }
    }
	string title = "";
	public string Title
	{
		get { return title; }
		set { title = value; }
	}
	public bool StartGame
	{
		get { return start; }
		set { start = value; }
	}
    public bool showStatusText = true;

    public string helpText = "";
    public bool showHelpButton = true;
    public bool showHelp = false;


    void Awake()
    {
        titleStyle = new GUIStyle( skin.label );
        titleStyle.alignment = TextAnchor.MiddleCenter;
        titleStyle.normal.textColor = titleColor;

        statusStyle = new GUIStyle( skin.label );
        statusStyle.alignment = TextAnchor.LowerCenter;

        helpStyle = new GUIStyle( skin.label );
        helpStyle.alignment = TextAnchor.UpperLeft;
        helpStyle.padding.left = 5;
        helpStyle.padding.right = 5;
    }

    #region Virtual Screen for automatic UI resolution scaling
    
    public static readonly float VirtualScreenWidth = 600;
    public static readonly float VirtualScreenHeight = 400;

    public static void ApplyVirtualScreen()
    {
        // resolution scaling matrix
        GUI.matrix = Matrix4x4.Scale( new Vector3( Screen.width / VirtualScreenWidth, Screen.height / VirtualScreenHeight, 1 ) );
    }

    #endregion

    protected virtual void OnGUI()
    {
        if( skin != null )
            GUI.skin = skin;

        ApplyVirtualScreen();

        GUI.Box( topBarRect, "" );
		if (!start) {
			if (GUI.Button (backButtonRect, "Начать")) {
				start = true;
			}
		}
        //if( GUI.Button( backButtonRect, "Сначала" ) )
        //    Application.LoadLevel( 0 );

        GUI.Label( titleRect, title );

        
		if (statusText == "Время вышло!") {
			if( GUI.Button( backButtonRect, "Сначала" ) )
            Application.LoadLevel( 0 );
		}
    }
}
