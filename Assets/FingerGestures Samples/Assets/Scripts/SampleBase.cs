using UnityEngine;
using System.Collections;

[RequireComponent( typeof( SampleUI ) )]
public class SampleBase : MonoBehaviour
{
    protected virtual string GetHelpText()
    {
        return "";
    }

    SampleUI ui;
    public SampleUI UI
    {
        get { return ui; }
    }

    protected virtual void Awake()
    {
        ui = GetComponent<SampleUI>();
    }

    protected virtual void Start()
    {
        ui.helpText = GetHelpText();
    }

    public static Vector3 GetWorldPos( Vector2 screenPos )
    {
        Ray ray = Camera.main.ScreenPointToRay( screenPos );
        float t = -ray.origin.z / ray.direction.z;

        return ray.GetPoint( t );
    }

}
